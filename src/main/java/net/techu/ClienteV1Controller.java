package net.techu;

import net.techu.data.ClienteMongo;
import net.techu.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

//@SpringBootApplication
public class ClienteV1Controller implements CommandLineRunner {

    @Autowired
    private ClienteRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(ClienteV1Controller.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Preparando MongoDB");
        List<ClienteMongo> lista = repository.findAll();
        for (ClienteMongo p:lista) {
            System.out.println(p.toString());
        }
    }
}

