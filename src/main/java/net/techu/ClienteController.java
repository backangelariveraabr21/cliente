package net.techu;

import net.techu.data.ClienteMongo;
import net.techu.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClienteController {
    @Autowired
    private ClienteRepository repository;
    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerListado() {
        List<ClienteMongo> lista = repository.findAll();
        return new ResponseEntity<List<ClienteMongo>>(lista, HttpStatus.OK);
    }

    @PostMapping(value="/clientes")
    public ResponseEntity<String> addCliente(@RequestBody ClienteMongo ClienteMongo)
    {
        ClienteMongo resultado = repository.insert(ClienteMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
    @PutMapping(value="/clientes/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ClienteMongo ClienteMongo)
    {
        Optional<ClienteMongo> resultado = repository.findById(id);
        if (resultado.isPresent()) {
            ClienteMongo productoAModificar = resultado.get();
            productoAModificar.nombre = ClienteMongo.nombre;
            productoAModificar.apellido = ClienteMongo.apellido;
            productoAModificar.ciudad = ClienteMongo.ciudad;
            ClienteMongo guardado = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
    }
    @DeleteMapping(value="/clientes/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable String id)
    {
        /* Posibilidad de buscar el cliente, después eliminarlo si existe */
        repository.deleteById(id);
        return new ResponseEntity<String>("Cliente borrado", HttpStatus.OK);
    }
}