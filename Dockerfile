FROM openjdk:15-jdk-alpine
COPY target/cliente-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/app.jar"]

